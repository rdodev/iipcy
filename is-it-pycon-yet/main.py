#!/usr/bin/env python

import os, json
from datetime import date
from flask import Flask, make_response

app = Flask(__name__)

@app.route('/')
def Welcome():
    pycon   = date(2018, 5, 11)
    now     = date.today()
    diff    = pycon - now
    iipcy   = pycon == now

    dtc = diff.days
    if dtc <= 0:
        dtc = 0

    tuts = diff.days - 2
    if tuts <= 0:
        tuts = 0


    out     = {'is-it-pycon-yet': iipcy,
               'days-until-conference': dtc,
               'europython2018': 'super awesome',
               'days-until-tutorials': tuts}
    resp = make_response(json.dumps(out), 200)
    resp.headers['Content-Type'] = 'application/json'
    return resp


if __name__ == "__main__":
	app.run(host='0.0.0.0', port=9000) #choose other port if need be
