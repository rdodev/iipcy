# Is-It-PyCon-Yet? As-a-Service
An unofficial countdown to PyCon US web service.

# Build using docker

`docker build <your tag name> .`

# Run using docker

`docker run -d -p 8000:8000 <your tag name>`